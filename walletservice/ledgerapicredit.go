package walletservice

import (
	"context"
	"fmt"
	"log"

	"github.com/jackc/pgx/v4/pgxpool"
)

//Selecting  for credited amount
func LedgerapiCredit(conn *pgxpool.Conn,Date string) ([]Getbalance, error) {
	creditbalance := []Getbalance{}
	fmt.Println("Coming to select Credit Amount")

	row, err := conn.Query(context.Background(), "select walletid,sum(amount) AS CREDIT_AMOUNT from wallet_api_ledger  where CAST(createddate AS DATE) = $1 AND TYPE=0 AND status='SUCCESS' GROUP BY walletid", Date)
	if err != nil {
		fmt.Println("error in selecting creditamount", err)
	}
	getbalance := Getbalance{}
	for row.Next() {

		if err := row.Scan(&getbalance.Walletid, &getbalance.Amount); err != nil {
			log.Fatal(err)
		}
		creditbalance = append(creditbalance, getbalance)

	}

	return creditbalance, nil
}
