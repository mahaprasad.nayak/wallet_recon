package walletservice

import (
	"context"
	"fmt"
	"log"

	"github.com/jackc/pgx/v4/pgxpool"
)

//Selecting closing balance
func returnClosingBalance(conn *pgxpool.Conn) ([]Getbalance, error) {
	closingbalance := []Getbalance{}
	fmt.Println("Coming to select Closing Balance")
	rows, err := conn.Query(context.Background(), "SELECT id,balance FROM wallet_api ")

	if err != nil {
		log.Fatal(err)
	}
	getbalance := Getbalance{}
	for rows.Next() {

		if err := rows.Scan(&getbalance.Walletid, &getbalance.Amount); err != nil {
			log.Fatal(err)
		}
		closingbalance = append(closingbalance, getbalance)

	}

	return closingbalance, nil
}
