package walletservice

type Auditreq struct {
	Date string
}
type Getbalance struct {
	Amount   float64
	Walletid int64
}
type Inserttest struct {
	Creditamount   float64
	Debitamount    float64
	Closingbalance float64
	Openingbalance float64
	Createddate    string
	Walletid       int64
	Id             int64
}

type Ledger struct {
	Flag                 bool
	Final_amount         float64
}

type Data struct {
	SendTo         string   `json:"sendTo"`
	CcTo           []string `json:"ccTo"`
	MessageSubject string   `json:"messageSubject"`
	MessageBody    string   `json:"messageBody"`
}

type Response struct {
	StatusDesc string `json:"message"`
	Status     bool   `json:"status"`
}
