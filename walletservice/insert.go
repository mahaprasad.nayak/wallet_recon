package walletservice

import (
	"context"
	"errors"
	"fmt"

	"github.com/jackc/pgx/v4/pgxpool"
)

func InsertAudit(conn *pgxpool.Pool, Date string) error {
	//fetching the ids for looping

	idconn, err := conn.Acquire(context.Background())
	if err != nil {
		fmt.Println("Inside Selecting ID:Exception in conn acquire:ID", err.Error())
		return errors.New(err.Error())
	}
	walletids, err := Getlistid(idconn)
	if err != nil {
		return err
	}
	idconn.Release()
	//connection for Creditamount
	credconn, err := conn.Acquire(context.Background())
	if err != nil {
		fmt.Println("Inside Selecting Credit Amount:Exception in conn acquire:insert general_api_ledger", err.Error())
		return errors.New(err.Error())
	}
	credamount, err := LedgerapiCredit(credconn, Date)
	if err != nil {
		return err
	}
	fmt.Println("List of Wallet ID's in Credit", credamount)
	credconn.Release()
	//connection for Debitamount
	debconn, err := conn.Acquire(context.Background())
	if err != nil {
		fmt.Println("Inside Selecting Debit Amount:Exception in conn acquire:insert general_api_ledger", err.Error())
		return errors.New(err.Error())
	}
	debitamount, err := LedgerapiDebit(debconn, Date)
	if err != nil {
		return err
	}
	fmt.Println("List of Wallet ID's in Debit", debitamount)
	debconn.Release()
	//connection for Closingbalance
	closeconn, err := conn.Acquire(context.Background())
	if err != nil {
		fmt.Println("Inside Selecting Closing Balance:Exception in conn acquire:insert general_api_ledger", err.Error())
		return errors.New(err.Error())
	}
	closingbalance, err := returnClosingBalance(closeconn)
	if err != nil {
		return err
	}
	fmt.Println("ClosingingBalance", closingbalance)
	closeconn.Release()
	
	for _, walletid := range walletids {
		insert := Inserttest{}
		insert.Walletid = walletid
		creditdata, isPresentcred := Isexits(walletid, credamount)
		if isPresentcred {
			insert.Creditamount = creditdata.Amount
		}
		debitdata, isPresentdebt := Isexits(walletid, debitamount)
		if isPresentdebt {
			insert.Debitamount = debitdata.Amount
		}
		
		closingbalancedata, isPresentclosingbalance := Isexits(walletid, closingbalance)
		if isPresentclosingbalance {
			insert.Closingbalance = closingbalancedata.Amount
		}

		//If No Credit debit happens
		if !isPresentcred && !isPresentdebt {
			fmt.Println("No credit and debit opreation found of wallet ID", walletid)
			continue
		}
		
		updateconn, err := conn.Acquire(context.Background())
		if err != nil {
			fmt.Println("Inside Insertwallet:Exception in conn acquire:update general_api_ledger audit", err.Error())
		}
		_,dberr := updateconn.Exec(context.Background(),"UPDATE general_api_ledger SET creditamount=$2,debitamount=$3,closingbalance=$4,updateddate=$5 where walletid=$1",walletid,insert.Creditamount,insert.Debitamount,insert.Closingbalance,Date)
		if dberr != nil {
			fmt.Println("Inside Insert test wallet", dberr)
			return dberr
		}
		updateconn.Release()

	}
	fmt.Println("updateded Successfully in general_api_ledger!!")
	
	return nil
}


//Check  the ID Present or not
func Isexits(id int64, listdata []Getbalance) (Getbalance, bool) {
	var selectdata Getbalance
	result := false
	for _, data := range listdata {
		if id == data.Walletid {
			result = true
			selectdata = data
			break
		}
	}
	return selectdata, result
}
