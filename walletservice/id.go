package walletservice

import (
	"context"
	"errors"
	"fmt"

	"github.com/jackc/pgx/v4/pgxpool"
)

//Selecting all Wallet ID of Partner
func Getlistid(conn *pgxpool.Conn) ([]int64, error) {
	var deviceSlice []int64
	row, err := conn.Query(context.Background(), "select walletid from general_api_ledger")
	if err != nil {
		fmt.Println("Error in selecting id", err)
		return deviceSlice, errors.New(err.Error())
	}
	for row.Next() {
		var walletid int64
		if err := row.Scan(&walletid); err != nil {
			fmt.Println("Error in getting List id", err)
		}
		deviceSlice = append(deviceSlice, walletid)
	}
	return deviceSlice, nil
}
