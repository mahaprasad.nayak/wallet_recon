package walletservice

import (
	"context"
	"fmt"
	"log"

	"github.com/jackc/pgx/v4/pgxpool"
)

//Selecting  for debit amount
func LedgerapiDebit(conn *pgxpool.Conn,Date string) ([]Getbalance, error) {
	fmt.Println("Coming to select Debit Amount")

	var debitbalance []Getbalance

	row, err := conn.Query(context.Background(), "select walletid,sum(amount) AS DEBIT_AMOUNT from wallet_api_ledger  where CAST(createddate AS DATE) = $1 AND TYPE=1 AND status='SUCCESS' GROUP BY walletid",Date)
	if err != nil {
		fmt.Println("error in selecting id", err)
	}
	getbalance := Getbalance{}
	for row.Next() {

		if err := row.Scan(&getbalance.Walletid, &getbalance.Amount); err != nil {
			log.Fatal(err)
		}
		debitbalance = append(debitbalance, getbalance)
	}

	return debitbalance, nil
}
