package main

//Delopyed in walletaudit # current code staging
import (
	"audit/audit"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"path/filepath"
	"time"

	"github.com/jackc/pgx/v4/pgxpool"
)

var conn *pgxpool.Pool

const (
	PORT1 = ":8080"
)

func main() {
	//Db connection
	absPath, _ := filepath.Abs("./app/stagingnode.crt")
	config, err := pgxpool.ParseConfig("postgres://reeturaj:reetu@35.184.195.70:26257/" +
		"isu_db?sslmode=verify-full&sslrootcert=" + absPath)
	// //Production
	// absPath, _ := filepath.Abs("../wallet_audit/app/prodnode.crt")
	// config, err := pgxpool.ParseConfig("postgres://iserveutech:6yYnF!xj3uZG9kXk@35.192.180.200:26257/" +
	// 	"defaultdb?sslmode=verify-full&sslrootcert=" + absPath)
	if err != nil {
		fmt.Println("error configuring the database: ", err)
		log.Fatal("error configuring the database: ", err)
	}
	conn, err = pgxpool.ConnectConfig(context.Background(), config)
	if err != nil {
		fmt.Println("error connecting to the database: ", err)
		log.Fatal("error connecting to the database: ", err)
	}

	defer conn.Close()
	fmt.Println("In Main")
	http.HandleFunc("/",getStatus)
	http.HandleFunc("/walletauditmanual", walletAudit)
	http.HandleFunc("/add", addId)
	err = http.ListenAndServe(":8080", nil)
	if err != nil {
		log.Fatal("Failed to start server", err)
	}

}
func getStatus(w http.ResponseWriter, r *http.Request) {
	io.WriteString(w, "Audit Application started.")
}
func walletAudit(w http.ResponseWriter, r *http.Request) {
	io.WriteString(w, "Audit Application started in Manual Mode.")
	//USER Audit
	loc, _ := time.LoadLocation("Asia/Kolkata")
	Date := time.Now().In(loc).Format("2006-01-02")
	fmt.Print("Audit Date::", Date, "of Zone :",loc, "\n")
	 resp,err := audit.WalletAudit(conn, Date)
	if err != nil {
		fmt.Println("Error while operating in Audit", err)
	}else{
		json.NewEncoder(w).Encode(resp)
	}
}
func addId(w http.ResponseWriter, r *http.Request) {
	io.WriteString(w, "API ID, balance add")
	loc, _ := time.LoadLocation("Asia/Kolkata")
	Date := time.Now().In(loc).Format("2006-01-02")
	resp,err:=audit.AddBalance(conn,Date)
	if err != nil {
		fmt.Println("Error while adding id, balance")
	}else{
		json.NewEncoder(w).Encode(resp)
	}
}
