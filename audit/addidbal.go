package audit

import (
	"audit/walletservice"
	"context"
	"fmt"
	"log"

	"github.com/jackc/pgx/v4/pgxpool"
)

func AddBalance(conn *pgxpool.Pool,Date string) (walletservice.Response,error) {
	var id int64
	var balance float64
	var count int64
	
	resp := walletservice.Response{}
	rows, err := conn.Query(context.Background(), "SELECT id,balance from wallet_api")
	if err != nil {
		log.Fatal(err)
		resp.StatusDesc = fmt.Sprint("Failed to added api_wallet_ids", count)
		resp.Status=false
	}
	for rows.Next() {
		if err := rows.Scan(&id,&balance); err != nil {
         	fmt.Println("Error in scanning id, balance",err)
        }
	if _,err:= conn.Exec(context.Background(),"INSERT INTO general_api_ledger(walletid,openingbalance,createddate) VALUES ($1,$2,$3)",id,balance,Date); err != nil {
		log.Fatal(err)
	}
	count++
}
	fmt.Println("APi_wallet_id added for the date",Date)
	resp.StatusDesc = fmt.Sprint("Total Api_wallet inserted ::", count)
	resp.Status=true
	return resp, nil

}