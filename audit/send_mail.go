package audit

import (
	"audit/walletservice"
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
)

const (
	recipientMail = "m.p.nayak2000@gmail.com"
	subject       = "Alert-Suspicious or Dubious Transaction on Wallet - [--SYSTEM GENERATED]"
)

var cc = []string{"sonalisatapathy1310@gmail.com", "sonali.satapathy@iserveu.in"}

func SendEmail(id string, Date string) error {

	fmt.Println("sending Email...")
	authAuthenticatorUrl := "https://awsnotification-vn3k2k7q7q-uc.a.run.app/send_email"
	//Set the values for audit MAIL
	mail := walletservice.Data{
		SendTo:         recipientMail,
		CcTo:           cc,
		MessageSubject: subject,
		MessageBody:    fmt.Sprintf("Dear Team, <br><br><br>Please check following Suspicious transaction at your end. <br>Wallet ID - " + id + "<br>For the date -" + Date + "<br><br> Please check urgently . "),
	}
	jsonValue, _ := json.Marshal(mail)
	_, err := http.Post(authAuthenticatorUrl, "application/json", bytes.NewBuffer(jsonValue))
	if err != nil {
		return errors.New(err.Error())
	}
	fmt.Println("Mail Sent Successfully")
	return nil
}

