package audit

import (
	"audit/walletservice"
	"context"
	"errors"
	"fmt"
	"math"
	"strconv"
	"time"

	"github.com/jackc/pgx/v4/pgxpool"
)

// Audit Function
func WalletAudit(conn *pgxpool.Pool, Date string) (walletservice.Response, error) {

	Time := time.Now().Format("15:04:05")
	err := walletservice.InsertAudit(conn, Date)
	var wallet_api []int64
	var presentCount int64

	resp := walletservice.Response{}

	if err == nil {

		//Selecting all details from general_api_ledger table for audit
		selconn, err := conn.Acquire(context.Background())
		if err != nil {
			fmt.Println("Inside Selecting ID:Exception in conn acquire:ID", err.Error())
			return resp, errors.New(err.Error())
		}
		rows, err := selconn.Query(context.Background(), "select debitamount,creditamount,closingbalance,openingbalance,walletid from general_api_ledger where createddate=$1",Date)
		if err != nil {
			fmt.Println("Error in get data for audit", err)
			return resp, err
		}
		selconn.Release()
		defer rows.Close()
		for rows.Next() {
			Audit := walletservice.Inserttest{}    //Audit amount to insert
			Audit_ledger := walletservice.Ledger{} //Audit Amount to send Mail

			if err := rows.Scan(&Audit.Debitamount, &Audit.Creditamount, &Audit.Closingbalance, &Audit.Openingbalance, &Audit.Walletid); err != nil {
				continue
			}
			fmt.Printf("CreditAmount %0.02f of walletid %d\n", Audit.Creditamount, Audit.Walletid)
			fmt.Printf("DebitAmount %0.02f of walletid %d\n", Audit.Debitamount, Audit.Walletid)
			fmt.Printf("openingbalance is %0.02f of walletid %d\n", Audit.Openingbalance, Audit.Walletid)
			fmt.Printf("closingbalance is %0.02f of walletid %d\n", Audit.Closingbalance, Audit.Walletid)
		
		
			// logic_PART Of AUDIT
			if Audit.Debitamount < Audit.Creditamount {
				Audit_ledger.Final_amount = Audit.Openingbalance + (Audit.Creditamount - Audit.Debitamount)

			} else if Audit.Debitamount > Audit.Creditamount {
				Audit_ledger.Final_amount = Audit.Openingbalance - (Audit.Debitamount - Audit.Creditamount)

			} else if Audit.Debitamount == Audit.Creditamount {
				Audit_ledger.Final_amount = Audit.Openingbalance

			}
			//&& math.Floor(Audit.Openingbalance*100)/100 == math.Floor(Audit_ledger.Prevday_closebalance*100)/100
			fmt.Println("Final Audit_amount ", math.Floor(Audit_ledger.Final_amount*100)/100, "of Wallet ID", Audit.Walletid)
			if math.Floor(Audit.Closingbalance*100)/100 == math.Floor(Audit_ledger.Final_amount*100)/100  {
				Audit_ledger.Flag = true
				fmt.Println("Flag", Audit_ledger.Flag)

			} else {
				Audit_ledger.Flag = false
				fmt.Println("Flag", Audit_ledger.Flag)
				id := strconv.FormatInt(Audit.Walletid, 10)
				//amount := fmt.Sprintf("%v", Audit_ledger.Final_amount)
				//flag1 := strconv.FormatBool(Audit_ledger.Flag)
				//calling Mail_api
				SendEmail(id, Date)
				wallet_api = append(wallet_api, Audit.Walletid)
				presentCount++
				resp.Status = false
			}

			inconn, err := conn.Acquire(context.Background())
			if err != nil {
				fmt.Println("Inside Selecting ID:Exception in conn acquire:ID", err.Error())
				return resp, errors.New(err.Error())
			}
			if _, err := inconn.Exec(context.Background(), "INSERT INTO audit_ledger(walletid,openingbalance,auditamount,closingbalance,date,time,flag) VALUES ($1,$2,$3,$4,$5,$6,$7)", Audit.Walletid, Audit.Openingbalance, Audit_ledger.Final_amount, Audit.Closingbalance, Date, Time, Audit_ledger.Flag); err != nil {
				fmt.Println("Error in insert audit data", err)
			} else {
				fmt.Println("Inserted transactions Successfully in audit_ledger!!!!!!")
			}
			inconn.Release()
		}
		// err = TerminateQuery(conn)
		// if err != nil {
		// 	fmt.Println("Error in insert audit data")
		// }
	}
	//Printing Message
	resp.StatusDesc = fmt.Sprint("Wallet Audit Success !! ", (presentCount), " Dispute Found in Wallet ID's ", wallet_api)

	return resp, nil
}
